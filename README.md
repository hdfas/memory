This project is a memory game written in JavaScript.
Use

The images are coming from  couchdb database just for demonstrating an API call [You can see the structure here](https://sofa.womo-stellplatz.eu/images/_all_docs/?include_docs=true).

## running
- yarn
- yarn run
- (yarn test)

## Used Libraries

- React 16.5.2
- React-redux
- redux
- redux-persist
- redux-store
- Jest
- Enzyme
- lodash


## Folder Structure

```
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    __tests__/
      index.test.js
    actions/
      index.js
      types.js
        __tests__/
          index.test.js
    reducers/
      __tests__/
        index.test.js      
      cards.js
      types.js
    App.css
    App.js
    App.test.js
    index.css
    index.js
    logo.svg
    setupTests.js
```
