import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from "enzyme";
import {App} from 'App';

let wrapped;

beforeEach(() => {
  wrapped = mount(
      <App />
  );
});

afterEach(() => {
  wrapped.unmount();
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("has an text input that users can type in", () => {
  expect(wrapped.find("input").prop("value")).toEqual(16);
});

it('should have a start button', () => {
  expect(wrapped.find("#startBtn").length).toEqual(1);
})
