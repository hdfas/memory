import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";
import cardsReducer from "reducers/cards";

const rootPersistConfig = {
  key: "root",
  storage: storage
};

const rootReducer = combineReducers({
  cards: cardsReducer
});

export default persistReducer(rootPersistConfig, rootReducer);
