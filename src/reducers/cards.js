import { START_GAME } from "actions/types";
import { GET_CARDS } from "actions/types";
import { MANAGE_CARDS } from "actions/types";
import { HIDE_CARDS } from "actions/types";
import { INCREMENT_PLAYER } from "actions/types";
import { INCREASE_SCORE } from "actions/types";
import { FETCH_IMAGES } from "actions/types";
import { FETCH_IMAGES_SUCCESS } from "actions/types";

import _ from "lodash";


const INITIAL_STATE = {
  images: [],
  cards: [],
  gameStarted: false,
  player: 1,
  nomatch: false,
  scoreBoard: []
};

const cardsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case START_GAME:
      const gameStarted = action.payload[0];
      const players = Number(action.payload[1]);
      // set up scoreboard
      let playerScore = [];
      for (let i = 1; i < players + 1; i++) {
        playerScore.push(Number(0));
      }

      return {
        ...state,
        gameStarted,
        players,
        player: 1,
        scoreBoard: playerScore,
        nomatch: false
      };

    // create cards object:
    // name: name of the card (2 * on the board)
    // open: indicates if the card is visible
    // matched: indicates if card was successfully matched
    // id: unique id
    case GET_CARDS:
      const cards = [];
      for (let i = 1; i < 3; i++) {
        for (let j = 0; j <= action.payload / 2 - 1; j++) {
          cards.push({
            name: j,
            open: false,
            matched: false,
            id: `${j}${i}`
          });
        }
      }

      let cardsShuffled = _.shuffle(cards);

      return {
        ...state,
        cards: cardsShuffled,
        cardsAmount: action.payload,
        selected: null
      };

    // manage card state ob board
    // payload: card object user clicked on

    case MANAGE_CARDS:
      const { id, matched, name, open, url } = action.payload;
      const cardIndex = state.cards.findIndex(i => i.id === id);
      let selected = name;
      let addpoint = false;
      let nomatch = false;

      // clone state.cards
      let newCards = {
        ...state,
        cards: {
          ...state.cards,
          [cardIndex]: {
            name,
            open: true,
            matched,
            id
          }
        }
      };

      newCards = Object.values(newCards.cards);


      if (state.selected !== null) { // a card was seleted before -> check match

        if (name === state.selected) { // match
          newCards = newCards.map(c => {
            c.matched = c.name === name || c.matched;
            return c;
          });
          addpoint = true; // increase point (this would be better done by directly dispatching the action from here)
        } else {
          nomatch = true; // set nomatch -> cards will be closed after 2 secs. (App.js 57)
        }
        selected = null; //reset selected card
      }

      return { ...state, cards: newCards, selected, nomatch, addpoint };

      /// iterate to all cards and leave just the matched ones open
    case HIDE_CARDS:
      let closedCards = state.cards.map(c => {
        c.open = c.matched;
        return c;
      });

      return { ...state, cards: closedCards, selected: null, nomatch: false };

    // add point (array state.scoreBoard )
    case INCREASE_SCORE:
      let sb = state.scoreBoard.map((p, i) => {
        if (i === state.player - 1) {
          return p + 1;
        }
        return p;
      });

      return { ...state, scoreBoard: sb, addpoint: false };

    // called in App.js:60 if cards were not matching.
    case INCREMENT_PLAYER:
      let player = Number(state.player);
      player = Number(state.player) < Number(state.players) ? player + 1 : 1;
      return { ...state, player };

    // get images from server
    case FETCH_IMAGES:
      const images = action.payload.images.rows;
      return { ...state, images, loading: true };

    // was supposed to display / hide a loader after successfully fetching images, not implemented due to deadline :(
    case FETCH_IMAGES_SUCCESS:
      return { ...state, loading: action.payload };

    default:
      return state;
  }
};

export default cardsReducer;
