import cardsReducer from "reducers/cards";
import { MANAGE_CARDS } from "actions/types";
import { HIDE_CARDS } from "actions/types";

it("handles actions of type MANAGE_CARDS (sets card to open and selected to card id )", () => {

  const card = {
    name: 1,
    open: false,
    matched: false,
    id: "11"
  };

  const action = {
    type: MANAGE_CARDS,
    payload: card
  };

  const cards = [
    {
      name: 1,
      open: false,
      matched: false,
      id: "11"
    },
    {
      name: 2,
      open: false,
      matched: false,
      id: "12"
    }
  ];

  const fooState = {
    cards: cards,
    nomatch: false,
    selected: null
  };

  const expState = {
    cards: [
      { id: "11", matched: false, name: 1, open: true },
      { id: "12", matched: false, name: 2, open: false }
    ],
    nomatch: false,
    selected: 1
  };

  const newState = cardsReducer(fooState, action);

  expect(newState).toEqual(expState);
});

it("handles actions of type HIDE_CARDS (sets all cards to open: false )", () => {
  const action = {
    type: HIDE_CARDS,
    payload: null
  };

  const cards = [
    {
      name: 1,
      open: false,
      matched: false,
      id: "11"
    },
    {
      name: 2,
      open: true,
      matched: false,
      id: "12"
    }
  ];

  const fooState = {
    cards: cards,
    nomatch: false,
    selected: null
  };

  const expState = {
    cards: [
      { id: "11", matched: false, name: 1, open: false },
      { id: "12", matched: false, name: 2, open: false }
    ],
    nomatch: false,
    selected: null
  };

  const newState = cardsReducer(fooState, action);

  expect(newState).toEqual(expState);
});

it("handles actions of unknown type", () => {
  const newState = cardsReducer([], { type: "UKNOWN" });
  expect(newState).toEqual([]);
});
