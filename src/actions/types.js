 // set up game with provided amount of cards and players
export const START_GAME = "start_game";
// populate cards
export const GET_CARDS = "get_cards";
// handle clicks on card (open, hide, add points, switch users)
export const MANAGE_CARDS = "manage_cards";
// close cards if not matching
export const HIDE_CARDS = "hide_cards";
// switch to next user if cards not matching
export const INCREMENT_PLAYER = "increment_player";
// increase score if cards matching
export const INCREASE_SCORE = "increase_score";
// fetch images from remote server
export const FETCH_IMAGES = "fetch_images";
// not implemented
export const FETCH_IMAGES_SUCCESS = "fetch_images_success";
