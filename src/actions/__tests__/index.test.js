import { getCards } from "actions";
import { manageCards } from "actions";
import { hideCards } from "actions";
import { GET_CARDS } from "actions/types";
import { MANAGE_CARDS } from "actions/types";
import { HIDE_CARDS } from "actions/types";

describe("getCards", () => {
  it("has the correct type", () => {
    const action = getCards();
    expect(action.type).toEqual(GET_CARDS);
  });

  it("has the correct payload", () => {
    const action = getCards(5);
    expect(action.payload).toEqual(5);
  });
});

describe("hideCards", () => {
  it("has the correct type", () => {
    const action = hideCards();
    expect(action.type).toEqual(HIDE_CARDS);
  });

  it("has the correct payload", () => {
    const action = hideCards();
    expect(action.payload).toEqual(null);
  });
});

describe("manageCards", () => {
  it("has the correct type", () => {
    const action = manageCards();
    expect(action.type).toEqual(MANAGE_CARDS);
  });

  it("has the correct payload", () => {
    const action = manageCards({
      name: 0,
      open: false,
      matched: false,
      id: `10`
    });
    expect(action.payload).toEqual({
      name: 0,
      open: false,
      matched: false,
      id: `10`
    });
  });
});
