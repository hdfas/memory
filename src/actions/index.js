import { START_GAME } from "actions/types";
import { GET_CARDS } from "actions/types";
import { MANAGE_CARDS } from "actions/types";
import { HIDE_CARDS } from "actions/types";
import { INCREMENT_PLAYER } from "actions/types";
import { INCREASE_SCORE } from "actions/types";
import { FETCH_IMAGES } from "actions/types";
import { FETCH_IMAGES_SUCCESS } from "actions/types";

export function startGame(gameState,players) {
  return {
    type: START_GAME,
    payload: [gameState,players]
  };
}

export function getCards(amount = 16) {
  return {
    type: GET_CARDS,
    payload: amount
  };
}

export function manageCards(card) {
  return {
    type: MANAGE_CARDS,
    payload: card
  };
}

export function increaseScore(player) {
  return {
    type: INCREASE_SCORE,
    payload: player
  };
}

export function incrementPlayer() {
  return {
    type: INCREMENT_PLAYER,
    payload: null
  };
}

export function hideCards() {
  return {
    type: HIDE_CARDS,
    payload: null
  };
}

export async function fetchImages( amount = 16) {
  try {
    const response = await fetch(
      "https://sofa.womo-stellplatz.eu/images/_all_docs?limit=" + amount + '&include_docs=true'
    );
    const images = await response.json();
    return {
      type: FETCH_IMAGES,
      payload: { images, amount, loading: false }
    };
  } catch (e) {
    console.error(e);
  }
}

export function fetchImagesSuccess() {
  return {
    type: FETCH_IMAGES_SUCCESS,
    payload: false
  }
}
