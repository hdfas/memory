import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "actions";
import "./App.css";
const INIT_CARDS_AMOUNT = 16;
const CARDS_MAX = 32;

export class App extends Component {
  state = {
    cardsAmount: INIT_CARDS_AMOUNT,
    players: 2,
    message: ""
  };

  componentDidMount() {
    this.getImages(this.state.cardsAmount);
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log(nextProps);
  // }
  // set up game , show cards
  renderCards = amount => {
    this.props.startGame(true, this.state.players);
    const { getCards } = this.props;
    const cards = this.props.getCards(amount);
  };

  getImages = async amount => {
    const images = await this.props.fetchImages(amount);
  };

  //
  handleAmountChange = event => {
    let amount = Number(event.target.value);
    this.setState({ message: "" });
    if (amount > CARDS_MAX || isNaN(amount)) {
      this.setState({
        message: "Please provide a card amount between 4 and 32"
      });
      return;
    }
    if (amount < 4) {
      this.setState({
        message: "The amount will be set to 4 if smaller then 4"
      });
    }
    if (amount % 2 !== 0) {
      this.setState({
        message: "The amount will be rounded to an even number"
      });
    }
    this.setState({ cardsAmount: amount });
  };

  handlePlayerChange = event => {
    let amount = Number(event.target.value);

    if (amount < 2 || amount > 4 || isNaN(amount)) {
      this.setState({
        message: "Please provide a players amount between 2 and 4"
      });
      return;
    }
    this.setState({ players: event.target.value });
  };

  resetGame = () => {
    this.props.startGame(false);
    this.setState({ cardsAmount: INIT_CARDS_AMOUNT, players: 2, message: "" });
  };

  onClick = card => {
    if (card.open || card.matched || this.props.nomatch) return;
    this.props.manageCards(card, this.props.player);

    setTimeout(() => {
      if (this.props.addpoint) {
        this.props.increaseScore(this.props.player);
      }
    },500);

    setTimeout(() => {
      if (this.props.nomatch) {
        this.props.hideCards();
        this.props.incrementPlayer();
      }
    }, 2000);
  };

  render() {
    return (
      <div className="App">
        {(!this.props.gameStarted && (
          <div>
            <h3 className="title">Welcome to Schmemory</h3>
            <div className="welcomeBox">
              <label>With how many cards should we play?</label>
              <input
                type="number"
                onChange={this.handleAmountChange}
                value={this.state.cardsAmount}
                max={CARDS_MAX}
              />
              <br />
              <label>How many players?</label>
              <input
                type="number"
                onChange={this.handlePlayerChange}
                value={this.state.players}
              />
            </div>
            <div>
              <button
                className="btnStart"
                id="startBtn"
                onClick={() => this.renderCards(this.state.cardsAmount)}
              >
                Start Game
              </button>
            </div>
            <span className="warning">{this.state.message}</span>
          </div>
        )) || (
          <div>
            <button id="resetBtn" onClick={() => this.resetGame()}>
              Reset Game
            </button>
            <ul>
              {this.props.scoreBoard.map((userScore, i) => {
                return (
                  <li key={i}>
                    Player {i + 1}: {userScore} Points
                  </li>
                );
              })}
            </ul>

            <h3>PLAYER {this.props.player}</h3>
            <hr />
            {this.props.cards.map((card, index) => {
              let bg = this.props.images.filter(i => {
                return Number(i.doc.card_id) === Number(card.name);
              });
              let bgimage = bg.length ? bg[0].doc.url : "";
              let lineBreak = index % (this.props.cardsAmount/4) === 0 ? "both" : "none";
              return (
                <div
                  className="card"
                  style={{
                    clear: lineBreak,
                    backgroundSize: "cover",
                    backgroundImage:
                      (card.open && `url(${bgimage})`) ||
                      `url("https://images.womo-stellplatz.eu/memory/def.jpg")`
                  }}
                  onClick={() => this.onClick(card)}
                  key={index}
                />
              );
            })}
          </div>
        )}
        {this.props.gameStarted && (
          <div style={{ clear: "both" }}>
            <hr />
            <button id="resetBtn" onClick={() => this.resetGame()}>
              Reset Game
            </button>
          </div>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return state.cards;
}

export default connect(
  mapStateToProps,
  actions
)(App);
